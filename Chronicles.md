# Purple Photons Chronicles

## TO DO:


* data from scientific publications on viral inactivation with 405nm light
* measure IRL of corona decay vs time
* decide the specs
* build the prototype

Side quest:
* LD from doubling of 808nm (gallium arsenide) infrared diode lasers?
* Is there laser at 270nm from doubling of 540nm green laser??..


## Weekly log

### 13-19 April 2020
The project is initiated by Mejdi, CEO of [IMPACT PHOTONICS](http://impact-photonics.com/).
