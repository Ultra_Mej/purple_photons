# Purple_Photons

Using light at 405nm ("Blu-Ray" laser diode) instead of light at 200-280nm (UV-C) for viral decontamination?  
This project is completely Open-Source. Go tell your friends!

## Technology Readiness Level
This project is currently at TRL 2 (it's just an idea)

## State of the art of UltraViolet Germicidal Irradiation
[UVGI](https://en.wikipedia.org/wiki/Ultraviolet_germicidal_irradiation) is an **old, field proven, FDA approved** technique that uses UV-C light (200-280nm) to inactivate a wide range of microorganisms.  
UV-C light kills virus by breaking (at the molecular level) their proteins and RNA.

In ref 1/ it is reported that radiant exposure **≥1 J/cm² of UV-C inactivates viruses similar to SARS-CoV-2 on a N95 mask**. I couldn't find the paper quoted "Mills et al., 2018; [2] Heimbuch & Harnish, 2019; [3] Lore et al., 2012; [4] Lin et al., 2018; [5] Fisher and Shaffer, 2010" to investigate (wavelength, level of inactivation, protocol...).

In ref 6/ the radiant exposure reported to reduce virus in cell is:
  * @ LED 285nm (p20/41): 3mJ/cm² for 1log reduction, 130mJ/cm² for 3log. Oguma et al. 2015
  * @ LED 255nm (p22/41): 14mJ/cm² for 1log, 38mJ/cm² for 3log. Aoyagi et al. 2011
  * @ LED 260nm (p23/41): 13mJ/cm² for 1log, 40mJ/cm² for 3log, 53mJ/cm² for 4log . Sholtes et al. 2016  
NB: Doesn't account for the penetration depth of light in material.  

In ref 7/ it is reported that **2mJ/cm² of 222nm light inactivating >95% of aerosolized H1N1 influenza virus**  

**NB: 1 J/cm² <=> 1W/cm² for 1s <=> 1mW/cm² for 20 minutes <=> 35µW/cm² for 8 hours**

Their are two kinds of UV-C lights sources used today in UVGI:

### Gas discharge light bulb
19th century technology. A gas (mercury for 250nm, excimers for more exotic/expensive choices...) trapped in a quartz bulb (glass absorbs UV light) is exited by an electric arc and emits UV light.
Examples:

[Light bulb](https://www.lighting.philips.com/main/prof/conventional-lamps-and-tubes/special-lamps/purificationwater-and-air/commercial-and-professional-air/tuv-pl-l/927908704007_EU/product) used in ref 2/:
  - 4 x 53 x 2cm, 130g
  - Main peak at 250nm
  - 17W radiant power => ~ 2mW/cm² at 20cm @ 55W power supply (100V, 500mA)
  - [30€/unit](https://www.lamps-on-line.com/tuv-pl-l-55w-4p-germicidal-pll-55w-2g11-uvc.html)
  - volume prices? 10€/unit? **!TBD!**

[Light bulb](https://www.amazon.fr/gp/product/B00XY9IUFG/) used in ref 4/:
  -  11 W lamp bulb from a “Sterilizer for Aquarium” kit
  -  similar specs as above. Optical power divided by 3?
  - [50€/unit](https://www.amazon.fr/gp/product/B00XY9IUFG/)

[Light bulb system](https://www.clordisys.com/torch.php) used in ref 3/:
  - 60 x 170 x 60cm, 30kg
  - 200µW/cm² at 3m @ 1kW power supply (220V/50Hz, 6A)
  - 25k€/unit


### UVC LED (Light Emitting Diode)
LED: 20th century technology. Electrons jump a bandgap and emit photons. The bandgap (the wavelength of the photon) is engineered by carefully doping semiconductors.  
UVC LED: 21st century technology. Aka "deep UV" LED. [AlGaN](https://en.wikipedia.org/wiki/Aluminium_gallium_nitride). Mostly used in the industry for UV curing. Examples:

[CUD7GF1A](https://www.neumueller.com/en/artikel/cud7gf1a)
  - 4 x 4 x 1mm, SMD package
  - Peak wavelength 280nm, FWHM 11nm, , 80% power in +/- 60°
  - Optical output power 2mW => ~20µW/cm² at 30mm (over 100cm²) @ 0.1W power supply (20mA, 6V forward voltage)
  - [9€/unit](https://store.nacsemi.com/Products/Detail?auth=n&utm_campaign=listing&part=CUD7GF1A&utm_medium=aggregator&Mfr=SEOUL%20VIOSYS%20CO.%20LTD&stock=XSFP00000084080&utm_source=findchips&utm_content=textlink)
  - volume prices? [5€/unit](https://www.aliexpress.com/i/33020883837.html?spm=2114.12057483.0.0.5bfc70a4YDNhaZ) **!TBC!**

[NCSU334A](https://www.led-professional.com/products/uv-ir-components/nichia-launches-a-280nm-deep-uv-led)
  - 7 x 7 x 2mm, 0.3g, SMD package
  - Peak wavelength 280nm, FWHM 10nm, 80% power in +/- 60°
  - "Radiant flux" 50mW  => ~0.4mW/cm² at 30mm (over 100cm²) @ 2W power supply (350mA, 5V forward voltage)
  - [160€/unit](https://www.aliexpress.com/i/33021503870.html) **what??**
  - volume prices?  **!TBD!**

[XST-3535-UV](https://www.luminus.com/products/uv)
  - 4 x 4 x 3mm, SMD package, integrated dome lens
  - Peak wavelength 280nm, FWHM 10nm, 80% power in +/- 30°
  - Output Radiant Power 45mW => ~3mW/cm² at 30mm (over 12cm²) @ 2W power supply (350mA, 5V to 7V forward voltage)
  - [24€/unit for 100 units](https://www.mouser.fr/ProductDetail/Luminus-Devices/XST-3535-UV-A60-CD275-00?qs=sGAEpiMZZMusoohG2hS%252B13XB79dZiCCbkzvwIpDy1yrlfg%2F1mtgJiw%3D%3D)

[RVXR-280](https://www.y-ic.com/datasheet/bc/RVXR-280-SB-073105.pdf)
  - 3 x 3 x 2.5mm, SMD package, integrated dome lens
  - Peak wavelength 280nm, FWHM 12nm, 80% power in +/- 60°
  - Output Radiant Power 8mW => ~100µW/cm² at 30mm (over 100cm²) @ 1W power supply (100mA, 7V forward voltage)
  - [6€/unit](https://www.digikey.fr/product-detail/en/rayvio-corporation/RVXR-280-SM-073105/1807-1023-1-ND/8635409?cur=EUR&lang=en)
  - volume prices?  **!TBD!**


## Opportunity of using 405nm light for viral decontamination
Though less common, other spectral bands can be used for germicidal irradiation of some microorganisms, including bacteria and virus:
* UV-B (280-315nm) **Example needed**
* UV-A (315-400nm) ~ NUV (300-400nm) **Example needed**
* Blue (400-480nm) **Example needed**  

Thanks to [Shuji Nakamura](https://en.wikipedia.org/wiki/Shuji_Nakamura) and its [GaN](https://en.wikipedia.org/wiki/Gallium_nitride) recipes (R&D in the 1990th, Nobel price in 2014),
we have access to cost-effective and high-power:
* Blue (380-480nm) LED. Mainly used to make white LED for the LED lighting market.
* 405nm Laser Diode. Mainly used as light source of [optical disks drive](https://en.wikipedia.org/wiki/Optical_disc_drive) for the optical storage market; and more recently as a light source for DIY/low-hand laser cutters.

**Consumer electronic market => high volume production by "many" manufacturers => low-cost.**  
**High tech + Low cost ?.. Frugal engineering opportunity detected! :)**  

In addition of their super low cost, 405nm laser diode are:
* More robust/less fragile than germicidal light bulbs.
* More powerful than UV LED. NB: but since their "germicidal power" will be less than UVC, more optical power will be needed.
* More directive than both: their light can easily be focused or projected.

### Example of 405nm Laser Diode:

[SLD3232VF](http://www.lillyelectronics.com/405nm-50mw-cw-violet-blue-laser-diode-ld-sld3232vf-sony-new)
  - 6 x 6 x 4mm, T0-18 package (radial throughhole)
  - beam divergence of +/-8°
  - Optical output power 50mW=> ~ 0.5mW/cm² over a 100cm² surface @ 0.2W power supply (50mA, 5V)
  - [8€/unit](http://www.lillyelectronics.com/405nm-50mw-cw-violet-blue-laser-diode-ld-sld3232vf-sony-new)
  - Volume prices: [2€/unit](https://www.aliexpress.com/item/32865935980.html?spm=a2g0o.productlist.0.0.4778766dkL4IAg&algo_pvid=1a9e046f-253d-4122-8957-3f284d7cfaf4&algo_expid=1a9e046f-253d-4122-8957-3f284d7cfaf4-11&btsid=0ab6f82415871118910117539e09a8&ws_ab_test=searchweb0_0,searchweb201602_,searchweb201603_)

[laser module for DIY engraving](https://www.aliexpress.com/item/32757820901.html?spm=a2g0o.productlist.0.0.4778766dkL4IAg&algo_pvid=1a9e046f-253d-4122-8957-3f284d7cfaf4&algo_expid=1a9e046f-253d-4122-8957-3f284d7cfaf4-5&btsid=0ab6f82415871118910117539e09a8&ws_ab_test=searchweb0_0,searchweb201602_,searchweb201603_)
  - 33 x 80 x 33mm, thermal management integrated.
  - Optical output power 500mW => ~ 10mW/cm² over a 50cm² surface (70x70mm) @ ?15W? power supply (12V, 1-2A)
  - [20€/unit](https://www.aliexpress.com/item/32757820901.html?spm=a2g0o.productlist.0.0.4778766dkL4IAg&algo_pvid=1a9e046f-253d-4122-8957-3f284d7cfaf4&algo_expid=1a9e046f-253d-4122-8957-3f284d7cfaf4-5&btsid=0ab6f82415871118910117539e09a8&ws_ab_test=searchweb0_0,searchweb201602_,searchweb201603_)


## End Game
A little irradiation box with BOM < 20€ and low electric consumption.  
To decontaminate small and flat objects that would be damaged by soap & water, such as:
  * filters of reusable masks
  * Disposable masks
  * Money bills
  * ...  

NB: Would also work as a UV curing-box for SLA 3D print.



--------------------------------------------------------------------------------
## Challenges
Can 405nm photons:
- kill virus?
- kill Corona virus?
- What radiant exposure needed?


## Theoretical investigation

### [Effects of 405 nm light on bacterial membrane integrity](https://www.microbiologyresearch.org/content/journal/micro/10.1099/mic.0.000350?crawler=true), 2016
* inactivation mechanism of 405 nm light has been accredited to the photoexcitation of intracellular photosensitive porphyrin molecules
* investigation into the specific mode of action has generated only limited results
* more recently, membrane damage has been indicated as having a role in microbial inactivation, with membrane degradation
* support of hypothesis is limited, and further confirmation of the mechanism of inactivation is required.
* Current study: effect of 405 nm light on bacterial cell membrane integrity?
* Sample volumes of 3 ml were positioned directly below the [LED array](http://www.photonstartechnology.com/Main_Upload/Innovate_Uno_+_Uno_Plus_Data_sheet.pdf), at a distance of 5 cm
* Irradiance of approximately 65mW/cm² at the sample surface
* <img src="/biblio/Sci/Bacteria_vs_405nm.jpg" width="500">
* **E.Colis: 5 log reduction @ ~ 0.4J/cm²**
* **S.Aureus: 4 à 5 log reduction @ ~0.2J/cm²**


### [New Proof-of-Concept in Viral Inactivation: Virucidal Efficacy of 405 nm Light Against Feline CaliciVirus as a Model for NoroVirus Decontamination](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5429381/), 2016
* NoV are one of the most common causes of epidemic acute gastroenteritis
* current knowledge on the antiviral efficacy of 405 nm light on medically important human and animal viruses is lacking and requires investigation.
* Current study: FCV was exposed to 405 nm light whilst suspended in minimal and organically-rich media
* the [LED array](http://www.photonstartechnology.com/Main_Upload/Innovate_Uno_+_Uno_Plus_Data_sheet.pdf) was at a distance of 4 cm from the microbial samples
* irradiance of 155.8mW/cm² at the sample surface
* **Antiviral activity with a 4 Log (99.99%) reduction in infectivity after a dose of 2.8 kJ/cm²**
* inactivate microorganisms via [ROS-generated oxidative damage](https://en.wikipedia.org/wiki/Reactive_oxygen_species)
* <img src="/biblio/Sci/FCVirus_vs_405nm.jpg" width="500">
* 2800J/cm² <=> 10mW/cm² for 2800/0.01/3600s = 80 hours (3 days)
* FCV exposed in artificial faeces, artificial saliva, blood plasma and other organically rich media exhibited an equivalent level of inactivation using between 50–85% less dose of the light.
* <img src="/biblio/Sci/FCVirus_in_OrganicRichMedia_vs_405nm.jpg" width="600">  
* 400J/cm² <=> 10mW/cm² for 11 hours (half a day)  

Is it possible to vaporise an "organically rich media"? Can a filter be made of an *organically rich media*?


### Is it only the J/cm² that maters or the pulsed light is better that DC light at equivalent power? (non linear DNA/RNA/Protein breakdown with light intensity)
**Under construction / Work needed**  

[Efficacy of Pulsed 405-nm Light-Emitting Diodes for Antimicrobial Photodynamic Inactivation: Effects of Intensity, Frequency, and Duty Cycle](https://www.researchgate.net/publication/309301352_Efficacy_of_Pulsed_405-nm_Light-Emitting_Diodes_for_Antimicrobial_Photodynamic_Inactivation_Effects_of_Intensity_Frequency_and_Duty_Cycle), 2016

### What is the absorption spectra of the SARS-CoV-2??
**Under construction / Work needed**  
It will absorb a lot bellow 300nm, ok. We now.  
But what about the NUV-VIS absorption spectra? Saturating a peak of absorption could initiate breakdown...


## Experimental investigation
**Under construction / Work needed**   
Can we study "virus decay vs time for different light irradiation parameters" in real-time with the instruments of the CBA?  
Parameters to be studied:
* wavelength: 405nm LED vs 280nm LED
* Continuous power: @ max DC supply and @ max/4
* Modulated power: @ 10% duty cycle for same DC (ie peak irradiance 10x higher)


--------------------------------------------------------------------------------
## References & Related projects

### 1/ https://www.n95decon.org/
A scientific consortium for data-driven study of N95 filtering facepiece respirator decontamination,
including [UVC irradiation](https://www.n95decon.org/s/200401_N95DECON_UV_factsheet_v12_final.pdf)

### 2/ https://www.frolicstudio.com/covid-19-decontamination-toolkit
Open-source DIY COVID-19 decontamination box using UVC

### 3/ https://www.nebraskamed.com/sites/default/files/documents/covid-19/n-95-decon-process.pdf
COVID-19 decontamination room using UVC

### 4/ http://www.needlab.org/face-masks-disinfection-device
Open-source Face-Mask Disinfection Device Using UV-C and Dry Heat

### 5/ http://www.iuva.org/COVID-19
Fact Sheet on UV Disinfection for COVID-19 by the IUVA "International" Ultraviolet Association

### 6/ https://www.iuvanews.com/stories/pdf/archives/180301_UVSensitivityReview_full.pdf
Review of the radian exposure needed for *Incremental Log Inactivation of Bacteria, Protozoa, Viruses and Algae needed IUVA*

### 7/ https://www.nature.com/articles/s41598-018-21058-w.pdf
Far-UVC light: A new tool to control the spread of airborne-mediated microbial diseases
